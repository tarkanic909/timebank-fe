import { CardComponent } from './components/card/card.component';
import { FormComponent } from './components/form/form.component';
import { AddjobComponent } from './components/addjob/addjob.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AgreementComponent } from './components/agreement/agreement.component';
import { GuestComponent } from './components/guest/guest.component';
import { AdminComponent } from './components/admin/admin.component';
import { LoginComponent } from './components/login/login.component';

const routes: Routes = [
  {
    path: '',
    component: GuestComponent,
    children: [
      { path: '', component: CardComponent },
      { path: 'login', component: LoginComponent },
    ],
  },
  {
    path: 'admin',
    component: AdminComponent,
    children: [
      { path: '', component: CardComponent },
      { path: 'jobs', component: CardComponent },
      { path: 'addjob', component: AddjobComponent },
      { path: 'agreement', component: AgreementComponent },
      { path: 'register', component: FormComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
