import { Job } from '../models/Job';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class JobsService {
  private apiUrl = 'http://104.248.254.24:8080';
  private apiGetJobs = this.apiUrl + '/jobs/autocomplete';
  private apiPostJobs = this.apiUrl + '/users/{id}/jobs';
  private apiJobAgreement = (id: string) =>
    this.apiUrl + '/jobs?provider=' + id;

  private apiFinishJob = (id: string) =>
    this.apiUrl + '/users/' + id + '/finished_jobs';

  constructor(private http: HttpClient) {}

  getJobs() {
    return this.http.get(this.apiGetJobs).pipe(map(this.remoteJobs));
  }

  remoteJobs(res: any): Job[] {
    let jobs: Job[] = [];

    for (let job of res) {
      jobs.push(
        new Job(
          job.id,
          job.provider,
          job.name,
          job.description,
          job.phoneNumber
        )
      );
    }

    return jobs;
  }

  addJob(job: Job) {
    return this.http.post(this.apiPostJobs, job);
  }

  getJobAgreement(id: string) {
    return this.http
      .get(this.apiJobAgreement(id))
      .pipe(map(this.jobAgreementMapping));
  }
  jobAgreementMapping(res: any) {
    return res;
  }

  finishJob(id: string, job: any) {
    return this.http.post(this.apiFinishJob(id), job);
  }
}
