import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthService {

  private isLogged: boolean = false;
  private fromWantJob: boolean = false;

  constructor() {}

  login(phonenumber: string, password: string) {
    //for testing
    this.isLogged = true;
    return new Observable((observer) => {
      observer.next(true);
    });
  }

  getIsLogged(){
    return this.isLogged;
  }

  getFromWantJob(){
    return this.fromWantJob;
  }

  setFromWantJob(){
    this.fromWantJob = true;
  }

  logout() {
    this.isLogged = false;
    this.fromWantJob = false;
  }
}
