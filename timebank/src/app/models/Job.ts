export class Job {
  constructor(
    public id: number,
    public provider: number,
    public name: string,
    public description: string,
    public phoneNumber: number
  ) {}
}
