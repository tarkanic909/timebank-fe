export class FinishedJob {
  constructor(
    //public id: number,
    public consumerId: string,
    public finishedId: string,
    public length: string,
    public finishedDate: string
  ) {}
}
