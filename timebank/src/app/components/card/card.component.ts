import { Component, OnInit } from '@angular/core';
import { Job } from 'src/app/models/Job';
import { JobsService } from 'src/app/services/job.service';
import { AuthService } from 'src/app/services/auth.service';
import {Router} from "@angular/router";

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css'],
})
export class CardComponent implements OnInit {
  public jobs: Job[];

  constructor(private jobsService: JobsService, private auth: AuthService, private router: Router,) {}

  ngOnInit(): void {
    this.load();
  }

  load() {
    console.log('Searching...');
    this.jobsService.getJobs().subscribe((jobsFromService) => {
      this.jobs = jobsFromService;
      console.log(jobsFromService);
    });
  }

  submitIWant(){
    if(this.auth.getIsLogged()){
      this.router.navigate(['/admin/agreement'])
    }else{
      this.auth.setFromWantJob();
      this.router.navigate(['/login'])
    }
  }
}
