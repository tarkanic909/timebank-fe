import { Component, OnInit } from '@angular/core';
import {Subscription} from "rxjs";
import {ActivatedRoute, Router} from "@angular/router";
import { JobsService } from 'src/app/services/job.service';
import {formatDate} from '@angular/common';
import {FinishedJob} from "../../models/FinishedJob";

@Component({
  selector: 'app-agreement',
  templateUrl: './agreement.component.html',
  styleUrls: ['./agreement.component.css']
})
export class AgreementComponent implements OnInit {

  userId: string = "1";

  public consumerId: string | "";
  public finishedId: string | "";
  public length: string | "";


  clicked = false;
  actualJob: any | {};
/*
  actualJob: any | undefined;
  agreementData :any = {id: 1, serviceName: "service_name1", workerName: "jozko", hours: 3, done: false};
*/
  private subscription: Subscription | undefined;

  constructor(
    private jobService: JobsService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    /*this.jobService.getJobAgreement(this.userId).subscribe((response: any) => {
      this.actualJob = response[0]
    })*/
  }

  createFinishedJobForService(): FinishedJob | undefined {
    {
      let finishedJob = new FinishedJob(
        this.consumerId,
        this.finishedId,
        this.length,
        formatDate(new Date(), 'yyyy-MM-ddThh:mm:ss.sssZ', 'en')
      )
      console.log("Finishing Job")
      return finishedJob;
    }
    return undefined;
  }

  submitJob(){
    let finishedJob = this.createFinishedJobForService();
    console.log(JSON.stringify(finishedJob));
    this.jobService.finishJob(this.userId, finishedJob).subscribe((res) => console.log(res))
  }

  onClickBack(){
    this.router.navigate(['/admin/jobs'])
  }



}
