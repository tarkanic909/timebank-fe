import { Component, OnInit } from '@angular/core';
import {Job} from "../../models/Job";
import {JobsService} from "../../services/job.service";
import {ActivatedRoute, Router} from "@angular/router";


@Component({
  selector: 'app-addjob',
  templateUrl: './addjob.component.html',
  styleUrls: ['./addjob.component.css']
})
export class AddjobComponent implements OnInit {
  public id = 0;
  public provider = 1;
  public name = '';
  public description = '';
  public phoneNumber = 948343573;

  constructor(
    private jobsService: JobsService,
    private router: Router
  ) { }


  ngOnInit(): void {

  }

  createJobForService(): Job | undefined {
    {
      let job = new Job(
        this.id,
        this.provider,
        this.name,
        this.description,
        this.phoneNumber
      )
      console.log("Adding New Job")
      return job;
    }
    return undefined;
  }


  addNewJob() {
    let job = this.createJobForService();
    if(job) {
      this.jobsService.addJob(job).subscribe((res) => {
        console.log("New Job Created");
        console.log(res);
        this.router.navigate(['/admin/jobs'])
      });
    }
  }


}
