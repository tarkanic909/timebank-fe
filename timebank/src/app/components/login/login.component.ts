import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  constructor(
    private fb: FormBuilder,
    private auth: AuthService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      phonenumber: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  onSubmit() {
    let { phonenumber, password } = this.loginForm.value;
    this.auth.login(phonenumber, password).subscribe({
      next: (isLogged) => {
        if(this.auth.getFromWantJob()){
          // when clicked i want and not logged user
          this.router.navigate(['/admin/agreement']);
        }else{
          // from login
          this.router.navigate(['/admin']);
        }

      },
    });
  }

  get phonenumber() {
    return this.loginForm.get('phonenumber') as FormControl;
  }
}
