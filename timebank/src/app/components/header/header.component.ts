import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  @Input() isAdmin: boolean = false;

  constructor(private router: Router, private auth: AuthService) {}

  ngOnInit(): void {}

  logout() {
    this.auth.logout();
    this.router.navigate(['/']);
  }
}
