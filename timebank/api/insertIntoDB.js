const { faker } = require("@faker-js/faker");
const fs = require("fs");

var database = { jobs: [], finished_jobs: []};

for (var i = 1; i <= 11; i++) {
  database.jobs.push({
    id: i,
    provider: i,
    name: faker.name.findName(),
    description: faker.lorem.paragraphs(2),
  });
}

for (var i = 1; i <= 10; i++) {
  database.finished_jobs.push({
    id: i,
    provider: i,
    name: faker.name.findName(),
    description: faker.lorem.paragraphs(2),
  });
}



var json = JSON.stringify(database);
fs.writeFile("./api/db.json", json, "utf8", (err) => {
  if (err) {
    console.error(err);
    return;
  }
  console.log("db.json created");
});
