const jsonServer = require("json-server");
const server = jsonServer.create();
const router = jsonServer.router("./api/db.json");
const middlewares = jsonServer.defaults();
const port = 8080;

server.use(middlewares);
server.use(
  jsonServer.rewriter({
    "/jobs/autocomplete": "/jobs",
    "/users/:id/jobs": "/jobs?provider=:id",
    "/users/:id/finished_jobs": "/finished_jobs",
  })
);

server.use(router);

server.listen(port, () => {
  console.log("JSON Server is running on PORT: " + port);
});
